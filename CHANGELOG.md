# ChangeLog

## Unreleased

**Changed:**
* Derived measurements from non-existing pre-conditions are set to false

## 2.1.2 (2025-01-30)

**Fixed:**
* Format MediaType alignment calculation fixed

## 2.1.1 (2025-01-30)

**Changed:**
* Revert last change ('phantom' distribution)

## 2.1.0 (2025-01-29)

**Changed:**
* Add 'phantom' distribution metrics to datasets that do not have a distribution 

## 2.0.8 (2024-xx-xx)

**Fixed:**

* fixed using the wrong vocabulary for the format, now `http://publications.europa.eu/resource/authority/file-type`
  should be used by default

## 2.0.7 (2024-07-01)

**Changed:**
* General update

## 2.0.6 (2024-02-14)

**Fixed:**
* Vert.x update ([CVE-2024-1300](https://access.redhat.com/security/cve/CVE-2024-1300))

## 2.0.5 (2024-02-01)

**Changed:**
* Lib dependency updates

## 2.0.4 (2024-01-31)

**Changed:**
* just for publish to gitlab.com

## 2.0.3 (2023-11-12)

**Changed:**
* Vert.x update

**Added:**
* helm chart

## 2.0.2 (2023-10-12)

**Fixed:**
* Instantiation of `annotationContext` map only once 

**Changed:**
* Refactor annotation calculation for better testing

## 2.0.1 (2023-07-14)

**Fixed:**
* JSON serialization issue with limited string size

## 2.0.0 (2023-05-26)

**Changed:**
* Complete refactoring

## 1.2.7 (2022-11-01)

**Changed:**
* Use fixed utilities for IanaTypes (old one caused memory issue)

## 1.2.5 (2021-11-24)

**Added:**
* Date modified annotation for datasets

## 1.2.4 (2021-10-18)

**Changed:**
* Important connector lib update

## 1.2.3 (2021-06-23)

**Changed:**
* Connector pipe handling

## 1.2.2 (2021-06-07)

**Fixed**
* Housekeeping

## 1.2.1 (2021-03-19)

**Fixed:**
* Vert.x issues (updated to 4.0.3)

## 1.2.0 (2021-01-31)

**Added:**
* `dqv:hasQualityMetadata` property for resource when metrics graph is created

**Changed:**
* Switched to Vert.x 4.0.0

## 1.1.0 (2020-04-23)

**Added:**
* Extended information about JSON-LD file-type

## 1.0.3 (2020-03-11)

**Changed:**
* Close dataset explicitly

## 1.0.2 (2020-03-06)

**Fixed:**
* Replace old measurements

## 1.0.1 (2020-03-04)

**Fixed:**
* Finally replace metrics graph to dataset

## 1.0.0 (2020-02-28)

Initial production release

**Added:**
* `attached` pipe config parameter
* Configuration `PIVEAU_LOAD_VOCABULARIES_FETCH`
* Configuration `EXCLUDE_ANNOTATIONS`
* Five Star `DQV.QualityAnnotation`
* Metric `atLeastFourStars`
* Annotate dcat ap compliance

**Changed:**
* Rearrange metadata quality
* Send rdf dataset with named graph
* Simplified pipe integration
* Use vocabularies library
* Annotations can now be excluded via configuration
* Measurements optionally replaceable
* Allow any name for metrics graph

**Removed:**
* Service proxy generation

**Fixed:**
* `dqv:computedOn`
* mediaType and format evaluation
* Test
