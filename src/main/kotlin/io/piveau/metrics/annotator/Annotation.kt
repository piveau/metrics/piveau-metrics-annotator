package io.piveau.metrics.annotator

import io.piveau.dcatap.DCATAPUriSchema
import io.piveau.metrics.annotator.annotations.*
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource
import org.apache.jena.rdf.model.ResourceFactory
import org.apache.jena.rdf.model.Statement
import org.apache.jena.vocabulary.DCAT

val datasetAnnotations = mutableListOf(
    DCATAPCompliancyAnnotation,
    AccessRightsAvailableAnnotation,
    AccessRightsVocabularyAlignmentAnnotation,
    ThemeAvailableAnnotation,
    ContactPointAvailableAnnotation,
    PublisherAvailableAnnotation,
    KeywordAvailableAnnotation,
    SpatialAvailableAnnotation,
    TemporalAvailableAnnotation,
    IssuedAvailableAnnotation,
    ModifiedAvailableAnnotation
)

// Keep in mind, order matters
val distributionAnnotations = mutableListOf(
    ByteSizeAvailableAnnotation,
    IssuedAvailableAnnotation,
    ModifiedAvailableAnnotation,
    DownloadURLAvailableAnnotation,
    RightsAvailableAnnotation,
    FormatAvailableAnnotation,
    MediaTypeAvailableAnnotation,
    LicenseAvailableAnnotation,
    KnownLicenseAnnotation,
    OpenLicenseAnnotation,
    MachineReadableAnnotation,
    NonProprietaryAnnotation,
    FormatMediaTypeVocabularyAlignmentAnnotation,
    FiveStarsAnnotation,
    AtLeastFourStarsAnnotation
)

fun calculateMetrics(dataset: Resource, dqvReport: Model) {
    val annotationContext = mutableMapOf<String, Any>()
    datasetAnnotations.forEach { annotation ->
        annotation.annotate(
            dataset,
            dqvReport,
            annotationContext
        )
    }
    val distributions: Set<Resource> = dataset.listProperties(DCAT.distribution)
        .mapWith { it.getObject().asResource() }
        .toSet()

    for (distribution in distributions) {
        distributionAnnotations.forEach { annotation ->
            annotation.annotate(
                distribution,
                dqvReport,
                annotationContext
            )
        }
    }
}
