package io.piveau.metrics.annotator

import io.piveau.dqv.createMetricsGraph
import io.piveau.dqv.listMetricsModels
import io.piveau.metrics.ApplicationConfig
import io.piveau.pipe.PipeContext
import io.piveau.rdf.*
import io.piveau.vocabularies.vocabulary.DQV
import io.piveau.vocabularies.vocabulary.OSI
import io.piveau.vocabularies.vocabulary.PV
import io.vertx.core.eventbus.Message
import io.vertx.kotlin.coroutines.CoroutineVerticle
import org.apache.jena.query.DatasetFactory
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.riot.Lang
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.RDF
import org.slf4j.LoggerFactory

class AnnotatorVerticle : CoroutineVerticle() {

    private val logger = LoggerFactory.getLogger(javaClass)

    override suspend fun start() {
        val excludeAnnotations = config.getString(ApplicationConfig.ENV_EXCLUDE_ANNOTATIONS, "")
            .splitToSequence(",")
            .map { when(it) {
                "isOpen" -> OSI.isOpen
                else -> ModelFactory.createDefaultModel().createResource("${PV.NS}$it")
            } }
            .toList()

        datasetAnnotations.removeAll { excludeAnnotations.contains(it.metric) }
        distributionAnnotations.removeAll { excludeAnnotations.contains(it.metric) }

        vertx.eventBus().consumer(ADDRESS) { message: Message<PipeContext> -> handlePipe(message) }
    }

    private fun handlePipe(message: Message<PipeContext>) = with(message.body()) {

        val result = when (val type = mimeType) {
            RDFMimeTypes.TRIG, RDFMimeTypes.TRIX, RDFMimeTypes.JSONLD -> stringData.toByteArray()
                .toDataset(type.asRdfLang())

            null -> DatasetFactory.create().apply {
                defaultModel = stringData.toByteArray().toModel(Lang.JSONLD)
            }

            else -> DatasetFactory.create().apply {
                defaultModel = stringData.toByteArray().toModel(type.asRdfLang())
            }
        }

        val model = result.defaultModel

        val it = model.listSubjectsWithProperty(RDF.type, DCAT.Dataset)
        if (it.hasNext()) {
            val dataset = it.next()
            val metricsModels = result.listMetricsModels()
            val metrics = if (metricsModels.isEmpty()) {
                val urn = ("urn:${pipe.header.context?.asNormalized() ?: "unknown"}:${pipe.header.name}")
                val metrics = result.createMetricsGraph(urn)
                metrics.add(dataset, DQV.hasQualityMetadata, metrics.getResource(urn))
            } else {
                metricsModels[0]
            }
            logger.debug("Annotating dataset [{}]", dataset.uri)
            calculateMetrics(dataset, metrics)

            log.info("Data annotated: {}", dataInfo)

            val output = result.presentAs(Lang.TRIG)

            setResult(
                output,
                RDFMimeTypes.TRIG,
                dataInfo.put("content", "metrics")
            ).forward()
        } else {
            setFailure("No dataset found in payload")
        }
    }

    companion object {
        const val ADDRESS = "io.piveau.pipe.annotating.dqv.queue"
    }

}