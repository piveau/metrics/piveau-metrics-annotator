package io.piveau.metrics.annotator.annotations

import io.piveau.dqv.replaceMeasurement
import io.piveau.vocabularies.vocabulary.PV
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Property
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms

open class AvailabilityAnnotation(private val property: Property, override val metric: Resource) : MetricsAnnotation() {

    override fun annotate(resource: Resource, report: Model, annotationContext: MutableMap<String, Any>) {
        report.replaceMeasurement(resource, metric, resource.hasProperty(property))
    }

}

object ByteSizeAvailableAnnotation : AvailabilityAnnotation(DCAT.byteSize, PV.byteSizeAvailability)
object AccessRightsAvailableAnnotation : AvailabilityAnnotation(DCTerms.accessRights, PV.accessRightsAvailability)
object ThemeAvailableAnnotation : AvailabilityAnnotation(DCAT.theme, PV.categoryAvailability)
object ContactPointAvailableAnnotation : AvailabilityAnnotation(DCAT.contactPoint, PV.contactPointAvailability)
object PublisherAvailableAnnotation : AvailabilityAnnotation(DCTerms.publisher, PV.publisherAvailability)
object KeywordAvailableAnnotation : AvailabilityAnnotation(DCAT.keyword, PV.keywordAvailability)
object SpatialAvailableAnnotation : AvailabilityAnnotation(DCTerms.spatial, PV.spatialAvailability)
object TemporalAvailableAnnotation : AvailabilityAnnotation(DCTerms.temporal, PV.temporalAvailability)
object IssuedAvailableAnnotation : AvailabilityAnnotation(DCTerms.issued, PV.dateIssuedAvailability)
object ModifiedAvailableAnnotation : AvailabilityAnnotation(DCTerms.modified, PV.dateModifiedAvailability)
object DownloadURLAvailableAnnotation : AvailabilityAnnotation(DCAT.downloadURL, PV.downloadUrlAvailability)
object RightsAvailableAnnotation : AvailabilityAnnotation(DCTerms.rights, PV.rightsAvailability)
object FormatAvailableAnnotation : AvailabilityAnnotation(DCTerms.format, PV.formatAvailability)
object MediaTypeAvailableAnnotation : AvailabilityAnnotation(DCAT.mediaType, PV.mediaTypeAvailability)
object LicenseAvailableAnnotation : AvailabilityAnnotation(DCTerms.license, PV.licenceAvailability)
