package io.piveau.metrics.annotator.annotations

import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource

abstract class MetricsAnnotation {

    abstract val metric: Resource

    abstract fun annotate(resource: Resource, report: Model, annotationContext: MutableMap<String, Any>)

}