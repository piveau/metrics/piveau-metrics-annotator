package io.piveau.metrics.annotator.annotations

import io.piveau.dcatap.MediaTypeVisitor
import io.piveau.dqv.replaceMeasurement
import io.piveau.vocabularies.vocabulary.PV
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.DCTerms

object FormatMediaTypeVocabularyAlignmentAnnotation : MetricsAnnotation() {

    override val metric: Resource
        get() = PV.formatMediaTypeVocabularyAlignment

    override fun annotate(resource: Resource, report: Model, annotationContext: MutableMap<String, Any>) {
        val knownFormat = resource.hasProperty(DCTerms.format) && annotationContext.containsKey("formatConcept")

        val knownMediaType = if (resource.hasProperty(DCAT.mediaType)) {
            val mediaTypeObject = resource.getProperty(DCAT.mediaType).getObject()
            mediaTypeObject.visitWith(MediaTypeVisitor) != null
        } else {
            false
        }
        report.replaceMeasurement(resource, metric, knownFormat || knownMediaType)
    }

}