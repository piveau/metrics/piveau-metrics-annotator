package io.piveau.metrics.annotator.annotations

import io.piveau.rdf.isRDF
import io.piveau.vocabularies.Concept
import io.piveau.vocabularies.vocabulary.DQV
import io.piveau.vocabularies.vocabulary.PROV
import io.piveau.vocabularies.vocabulary.PV
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.RDFNode
import org.apache.jena.rdf.model.Resource
import org.apache.jena.vocabulary.OA
import org.apache.jena.vocabulary.RDF

object FiveStarsAnnotation : MetricsAnnotation() {

    override val metric: Resource
        get() = PV.fiveStars

    override fun annotate(resource: Resource, report: Model, annotationContext: MutableMap<String, Any>) {
        val fiveStarsAnnotation: Resource = report.createResource(DQV.QualityAnnotation)

        // 1. is open
        // 2. machine-readable
        // 3. non propriety
        // 4. is RDF
        // 5. RDF contains URIRefs to external resources
        if (annotationContext.containsKey("openLicenseMeasurement")) {
            fiveStarsAnnotation.addProperty(PROV.wasDerivedFrom, annotationContext["openLicenseMeasurement"] as RDFNode?)
        }
        val isOpen = annotationContext.getOrDefault("isOpen", false) as Boolean
        if (!isOpen) {
            finalizeAnnotation(report, resource, fiveStarsAnnotation, PV.zeroStars, annotationContext)
            return
        }
        if (annotationContext.containsKey("machineReadableMeasurement")) {
            fiveStarsAnnotation.addProperty(PROV.wasDerivedFrom, annotationContext["machineReadableMeasurement"] as RDFNode?)
        }
        val isMachineReadable = annotationContext.getOrDefault("isMachineReadable", false) as Boolean
        if (!isMachineReadable) {
            finalizeAnnotation(report, resource, fiveStarsAnnotation, PV.oneStar, annotationContext)
            return
        }
        if (annotationContext.containsKey("nonProprietaryMeasurement")) {
            fiveStarsAnnotation.addProperty(PROV.wasDerivedFrom, annotationContext["nonProprietaryMeasurement"] as RDFNode?)
        }
        val isNonProprietary = annotationContext.getOrDefault("isNonProprietary", false) as Boolean
        if (!isNonProprietary) {
            finalizeAnnotation(report, resource, fiveStarsAnnotation, PV.twoStars, annotationContext)
            return
        }
        if (!annotationContext.containsKey("formatConcept") || !isRDF(annotationContext["formatConcept"] as Concept)) {
            finalizeAnnotation(report, resource, fiveStarsAnnotation, PV.threeStars, annotationContext)
            return
        }
        finalizeAnnotation(report, resource, fiveStarsAnnotation, PV.fourStars, annotationContext)

        // 5. will be added later
    }

    private fun finalizeAnnotation(
        report: Model,
        resource: Resource,
        fiveStarsAnnotation: Resource,
        fiveStarsRating: Resource,
        annotationContext: MutableMap<String, Any>
    ) {
        fiveStarsAnnotation.addProperty(RDF.type, DQV.QualityAnnotation)
        fiveStarsAnnotation.addProperty(OA.hasBody, fiveStarsRating)
        fiveStarsAnnotation.addProperty(OA.motivatedBy, OA.classifying)
        report.add(resource, DQV.hasQualityAnnotation, fiveStarsAnnotation)
        annotationContext["fiveStars"] = fiveStarsRating
    }

    private fun isRDF(formatConcept: Concept) = true
}