package io.piveau.metrics

import io.piveau.metrics.annotator.AnnotatorVerticle
import io.piveau.pipe.connector.PipeConnector
import io.piveau.vocabularies.initRemotes
import io.vertx.config.ConfigRetriever
import io.vertx.core.DeploymentOptions
import io.vertx.core.Launcher
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait

class MainVerticle : CoroutineVerticle() {

    override suspend fun start() {
        val loadConfig = ConfigRetriever.create(vertx).config.coAwait()

        // Config values

        initRemotes(vertx, remotes = false, prefetch = false)
        vertx.deployVerticle(AnnotatorVerticle::class.java, DeploymentOptions().setConfig(loadConfig)).coAwait()

        PipeConnector.create(vertx)
            .coAwait()
            .publishTo(AnnotatorVerticle.ADDRESS, false)
    }

}

fun main(args: Array<String>) = Launcher.executeCommand("run", *(args.plus(MainVerticle::class.java.name)))
