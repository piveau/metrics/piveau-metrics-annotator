package io.piveau.metrics.annotator

import io.piveau.dqv.createMetricsGraph
import io.piveau.rdf.presentAs
import io.piveau.rdf.toModel
import io.piveau.vocabularies.vocabulary.DQV
import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.apache.jena.query.DatasetFactory
import org.apache.jena.rdf.model.Model
import org.apache.jena.riot.Lang
import org.apache.jena.vocabulary.DCAT
import org.apache.jena.vocabulary.RDF
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.slf4j.LoggerFactory

@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class AnnotationsTest {

    private val log = LoggerFactory.getLogger(javaClass)

    @Test
    fun testDatasetAnnotation(vertx: Vertx, testContext: VertxTestContext) {
        vertx.fileSystem().readFile("test.rdf")
            .onSuccess { buffer ->
                val model: Model = buffer.bytes.toModel(Lang.RDFXML)
                val dataset = DatasetFactory.create(model)
                val urn = "urn:example:test"
                val metrics = dataset.createMetricsGraph(urn)
                val resource = model.listSubjectsWithProperty(RDF.type, DCAT.Dataset).next()
                metrics.add(resource, DQV.hasQualityMetadata, metrics.getResource(urn))

                calculateMetrics(resource, metrics)

//                log.info(metrics.presentAs(Lang.TURTLE))
                testContext.completeNow()
            }
            .onFailure(testContext::failNow)
    }
}
