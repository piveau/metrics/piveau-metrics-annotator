<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <groupId>io.piveau.metrics</groupId>
    <artifactId>annotator</artifactId>
    <version>2.1.3</version>

    <name>piveau-metrics-annotator</name>

    <properties>
        <kotlin.version>2.1.0</kotlin.version>
        <kotlin.code.style>official</kotlin.code.style>
        <kotlin.compiler.incremental>true</kotlin.compiler.incremental>
        <vertx.version>4.5.12</vertx.version>
        <vertx.verticle>io.piveau.metrics.MainVerticle</vertx.verticle>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.build.timestamp.format>yyyy-MM-dd HH:mm</maven.build.timestamp.format>
        <maven.compiler.release>17</maven.compiler.release>
        <buildTimestamp>${maven.build.timestamp}</buildTimestamp>
    </properties>

    <scm>
        <!--suppress UnresolvedMavenProperty -->
        <url>${CI_PROJECT_URL}</url>
        <!--suppress UnresolvedMavenProperty -->
        <developerConnection>
            scm:git:https://${GITLAB_USER}:${GITLAB_TOKEN}@${CI_SERVER_FQDN}/${CI_PROJECT_PATH}.git
        </developerConnection>
        <tag>2.1.3</tag>
    </scm>

    <repositories>
        <repository>
            <id>paca</id>
            <name>paca</name>
            <url>https://paca.fokus.fraunhofer.de/repository/maven-public/</url>
        </repository>
    </repositories>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>io.vertx</groupId>
                <artifactId>vertx-stack-depchain</artifactId>
                <version>${vertx.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <dependencies>

        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-stdlib</artifactId>
            <version>${kotlin.version}</version>
        </dependency>

        <!-- VertX -->

        <dependency>
            <groupId>io.vertx</groupId>
            <artifactId>vertx-core</artifactId>
        </dependency>
        <dependency>
            <groupId>io.vertx</groupId>
            <artifactId>vertx-config</artifactId>
        </dependency>
        <dependency>
            <groupId>io.vertx</groupId>
            <artifactId>vertx-web-api-contract</artifactId>
        </dependency>
        <dependency>
            <groupId>io.vertx</groupId>
            <artifactId>vertx-web-client</artifactId>
        </dependency>
        <dependency>
            <groupId>io.vertx</groupId>
            <artifactId>vertx-health-check</artifactId>
        </dependency>

        <dependency>
            <groupId>io.piveau.pipe</groupId>
            <artifactId>pipe-connector</artifactId>
            <version>8.0.3</version>
        </dependency>

        <!-- RDF -->

        <dependency>
            <groupId>io.piveau.utils</groupId>
            <artifactId>piveau-utils</artifactId>
            <version>11.3.1</version>
        </dependency>

        <!-- Logging -->

        <dependency>
            <groupId>org.codehaus.janino</groupId>
            <artifactId>janino</artifactId>
            <version>3.1.12</version>
        </dependency>
        <dependency>
            <groupId>net.logstash.logback</groupId>
            <artifactId>logstash-logback-encoder</artifactId>
            <version>7.4</version>
        </dependency>
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>1.5.6</version>
        </dependency>

        <!-- Testing -->

        <dependency>
            <groupId>io.vertx</groupId>
            <artifactId>vertx-junit5</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>

        <finalName>${project.artifactId}</finalName>

        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>

        <sourceDirectory>src/main/kotlin</sourceDirectory>
        <testSourceDirectory>src/test/kotlin</testSourceDirectory>

        <plugins>
            <plugin>
                <groupId>org.jetbrains.kotlin</groupId>
                <artifactId>kotlin-maven-plugin</artifactId>
                <version>${kotlin.version}</version>
                <executions>
                    <execution>
                        <id>compile</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>test-compile</id>
                        <phase>test-compile</phase>
                        <goals>
                            <goal>test-compile</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <jvmTarget>17</jvmTarget>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.13.0</version>
            </plugin>
            <plugin>
                <artifactId>maven-release-plugin</artifactId>
                <version>3.1.1</version>
                <configuration>
                    <tagNameFormat>@{project.version}</tagNameFormat>
                </configuration>
            </plugin>
            <plugin>
                <groupId>io.reactiverse</groupId>
                <artifactId>vertx-maven-plugin</artifactId>
                <version>1.0.28</version>
                <executions>
                    <execution>
                        <id>vmp</id>
                        <goals>
                            <goal>initialize</goal>
                            <goal>package</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <redeploy>true</redeploy>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.2.5</version>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>0.8.12</version>
                <executions>
                    <execution>
                        <id>prepare-agent</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>report</id>
                        <phase>test</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>license-maven-plugin</artifactId>
                <version>2.4.0</version>
                <configuration>
                    <skipAggregateAddThirdParty>false</skipAggregateAddThirdParty>
                    <outputDirectory>${project.basedir}/third-party</outputDirectory>
                </configuration>
                <executions>
                    <execution>
                        <id>download-licenses</id>
                        <goals>
                            <goal>add-third-party</goal>
                            <goal>third-party-report</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

</project>
